package in.iolite.employee_management.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import in.iolite.employee_management.exception.ResourceNotFoundException;
import in.iolite.employee_management.model.Employee;
import in.iolite.employee_management.repository.EmployeeRepository;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public Employee createEmployee(Employee employee) {
		logger.info("invoked EmployeeController.createEmployee()");
		try {
			return employeeRepository.save(employee);
		   } 
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		  }
		return employee;
	}

	@Override
	public Employee getEmployeeById(Long employeeId) throws ResourceNotFoundException {
		logger.info("invoked EmployeeController.getEmployeeById()");
		Employee employee = null;

		employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));

		return employee;

	}

	@Override
	public Employee updateEmployee(Long employeeId, Employee employeeDetails) throws ResourceNotFoundException {
		logger.info("invoked EmployeeController.updateEmployee()");
		Employee employee = null;

		employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));

		employee.setEmailId(employeeDetails.getEmailId());
		employee.setLastName(employeeDetails.getLastName());
		employee.setFirstName(employeeDetails.getFirstName());
		final Employee updatedEmployee = employeeRepository.save(employee);
		return updatedEmployee;

	}

	@Override
	public Boolean deleteEmployee(Long employeeId) throws ResourceNotFoundException {
		logger.info("invoked EmployeeController.deleteEmployee()");
		Employee employee;

		employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
		employeeRepository.delete(employee);
		return true;
	}

	@Override
	public List<Employee> getAllEmployees() {
		logger.info("invoked EmployeeController.getAllEmployees()");
		return employeeRepository.findAll();
	}

}
