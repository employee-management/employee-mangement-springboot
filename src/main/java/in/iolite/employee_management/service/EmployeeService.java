package in.iolite.employee_management.service;
import java.util.List;
import in.iolite.employee_management.exception.ResourceNotFoundException;
import in.iolite.employee_management.model.Employee;


public interface EmployeeService {
	
	public List<Employee> getAllEmployees() throws ResourceNotFoundException;

	public Employee createEmployee(Employee employee);
	
	public Employee getEmployeeById(Long employeeId) throws ResourceNotFoundException;
	
	public Employee updateEmployee(Long employeeId,Employee employeeDetails) throws ResourceNotFoundException;
	
	public Boolean deleteEmployee(Long employeeId) throws ResourceNotFoundException;
}
