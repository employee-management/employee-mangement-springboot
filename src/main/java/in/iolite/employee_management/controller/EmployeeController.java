package in.iolite.employee_management.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.iolite.employee_management.exception.ResourceNotFoundException;
import in.iolite.employee_management.model.Employee;
import in.iolite.employee_management.service.EmployeeService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	private Logger logger = LoggerFactory.getLogger(getClass());

	@GetMapping("/getAllEmployees")
	public List<Employee> getAllEmployees() {
		logger.info("invoked EmployeeController.getAllEmployees()");
		try {
			return employeeService.getAllEmployees();
		} catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	@GetMapping("/getEmployee/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId) {
		logger.info("invoked EmployeeController.getEmployeeById()");
		Employee employee;
		try {
			employee = employeeService.getEmployeeById(employeeId);
			if (Objects.nonNull(employee)) {
				return ResponseEntity.ok().body(employee);
			}
		 } 
		catch (ResourceNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	@PostMapping("/addEmployee")
	public Employee createEmployee(@Valid @RequestBody Employee employee) {
		logger.info("invoked EmployeeController.createEmployee()");
		try {
			return employeeService.createEmployee(employee);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return employee;

	}

	@PutMapping("/updateEmployee/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody Employee employeeDetails) throws ResourceNotFoundException {
		logger.info("invoked EmployeeController.updateEmployee()");
		try {
			Employee updatedEmployee = employeeService.updateEmployee(employeeId, employeeDetails);
			if (Objects.nonNull(updatedEmployee)) {
				return ResponseEntity.ok(updatedEmployee);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}

	@DeleteMapping("/deleteEmployee/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		logger.info("invoked EmployeeController.deleteEmployee()");
		Map<String, Boolean> response = null;
		try {
			Boolean result = employeeService.deleteEmployee(employeeId);
			response = new HashMap<>();
			response.put("deleted", result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return response;
	}
}
